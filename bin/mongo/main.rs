#![allow(dead_code)]

use std::path;

mod runner;

#[tokio::main]
async fn main() {
    let config = db_tool::Config::from(path::Path::new("./config"))
        .await
        .unwrap();
    let database = &config.env_config.mongodb.database.iot.clone();
    let client = db_tool::new_client(config).unwrap();

    // 启动任务
    runner::Runner::new(client.database(database))
        .run()
        .await
        .unwrap()
}
