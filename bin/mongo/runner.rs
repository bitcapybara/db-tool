use std::time::Instant;

use bson::bson;
use chrono::{Duration, Local, TimeZone};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("mongo error: {0:#}")]
    Mongo(#[from] mongodb::error::Error),
    #[error("bson error: {0:#}")]
    Bson(#[from] bson::ser::Error),
    #[error("chrono error: {0:#}")]
    Chrono(#[from] chrono::format::ParseError),
}

pub struct Runner {
    collection: mongodb::Collection<()>,
}

impl Runner {
    pub fn new(db: mongodb::Database) -> Self {
        Self {
            collection: db.collection("t_device_logs"),
        }
    }

    pub async fn run(self) -> Result<(), Error> {
        let mut ts = Local.datetime_from_str("2022-07-17 00:00:00", "%Y-%m-%d %H:%M:%S")?;

        let end = Local::now() - Duration::days(30);

        let mut interval_hour = 1;

        loop {
            let start = Instant::now();

            println!("{}", ts);

            if ts >= end {
                break;
            }
            ts += Duration::hours(interval_hour);

            let filter = bson!({
                "timestamp": {
                    "$lt": bson::DateTime::from(ts),
                }
            });
            let doc = bson::to_document(&filter)?;

            let count = self.collection.count_documents(doc.clone(), None).await?;
            println!("find: {}, interval: {}", count, interval_hour);
            if count == 0 {
                ts += Duration::days(1);
                continue;
            }

            match count {
                n if n > 50_0000 => {
                    interval_hour -= 1;
                    continue;
                }
                n if n < 10_0000 => {
                    interval_hour += 1;
                    continue;
                }
                _ => {
                    if interval_hour < 0 {
                        interval_hour = 1;
                    }
                }
            }

            let res = self.collection.delete_many(doc, None).await?;
            println!("deleted {}", res.deleted_count);

            println!("duration {:?}", start.elapsed());

            tokio::time::sleep(tokio::time::Duration::from_secs(10)).await;
        }
        Ok(())
    }
}
