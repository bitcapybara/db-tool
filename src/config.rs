use std::path;

use tokio::{
    fs,
    io::{self, AsyncReadExt},
};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("io error: {0:#}")]
    IO(#[from] io::Error),
    #[error("parse error: {0:#}")]
    Parse(#[from] toml::de::Error),
}

#[derive(Debug, serde::Deserialize)]
pub struct Config {
    pub env_config: EnvConfig,
}

#[derive(Debug, serde::Deserialize)]
pub struct DefaultConfig {
    pub env: Env,
}

#[derive(Debug, serde::Deserialize)]
pub enum Env {
    #[serde(rename = "dev")]
    Dev,
    #[serde(rename = "test")]
    Test,
    #[serde(rename = "pre")]
    Pre,
    #[serde(rename = "prod")]
    Prod,
}

#[derive(Debug, serde::Deserialize)]
pub struct EnvConfig {
    pub mongodb: MongoConfig,
}

#[derive(Debug, serde::Deserialize)]
pub struct MongoConfig {
    pub addr: Vec<String>,
    pub username: String,
    pub password: String,
    pub auth_source: String,
    pub replica_set: String,
    pub database: Database,
}

#[derive(Debug, serde::Deserialize)]
pub struct Database {
    pub iot: String,
}

impl Config {
    pub async fn from(path: &path::Path) -> Result<Self, Error> {
        // 首先，获取默认配置文件
        let content = read_file(path.join("default.toml")).await?;
        let default_config = toml::from_str::<DefaultConfig>(&content)?;

        let env_file = match default_config.env {
            Env::Dev => "dev.toml",
            Env::Test => "test.toml",
            Env::Pre => "pre.toml",
            Env::Prod => "prod.toml",
        };

        let env_config = EnvConfig::from(path.join(env_file)).await?;

        Ok(Self { env_config })
    }
}

impl EnvConfig {
    pub async fn from(path: impl AsRef<path::Path>) -> Result<Self, Error> {
        let content = read_file(path).await?;

        Ok(toml::from_str::<EnvConfig>(&content)?)
    }
}

async fn read_file(path: impl AsRef<path::Path>) -> Result<String, Error> {
    let mut file = fs::File::open(path).await?;
    let mut content = String::new();
    file.read_to_string(&mut content).await?;
    Ok(content)
}

#[cfg(test)]
mod tests {

    use super::*;

    #[tokio::test]
    async fn config_work() {
        let config = Config::from(path::Path::new("config")).await;
        assert!(config.is_ok())
    }
}
