use std::str::FromStr;

use mongodb::{
    options::{self, Credential, ServerAddress},
    Client,
};

use crate::config::{self, Config};

#[derive(Debug, thiserror::Error)]
pub enum Error {
    #[error("mongo error: {0:#}")]
    Mongo(#[from] mongodb::error::Error),
    #[error("config error: {0:#}")]
    Config(#[from] config::Error),
}

pub fn new_client(config: Config) -> Result<Client, Error> {
    let cfg = config.env_config.mongodb;
    // mongo 集群地址
    let address = cfg
        .addr
        .iter()
        .map(|s| ServerAddress::from_str(s).unwrap())
        .collect::<Vec<ServerAddress>>();
    // mongo 认证信息
    let credential = Credential::builder()
        .source(cfg.auth_source)
        .username(cfg.username)
        .password(cfg.password)
        .build();
    // 参数构建
    let opts = options::ClientOptions::builder()
        .hosts(address)
        .credential(Some(credential))
        .repl_set_name(cfg.replica_set)
        .build();

    Ok(Client::with_options(opts)?)
}
